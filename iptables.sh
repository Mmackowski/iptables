iptables -F
iptables -t nat -F
iptables -t mangle -F
#change enp4s2 to external network interface
iptables -t nat -A POSTROUTING -o enp4s2 -j MASQUERADE
#change enp0s25 to internal interface
iptables -A FORWARD -i enp4s2 -o enp0s25 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i enp0s25 -o enp4s2 -j ACCEPT
